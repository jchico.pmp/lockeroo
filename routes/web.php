<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::delete('/carts/empty', 'CartController@empty')->name('carts.empty');

Route::put('/transactions/update_ps', 'TransactionController@update_ps')->name('transactions.update_ps');

Route::resource('products', 'ProductController');
Route::resource('locations', 'LocationController');
Route::resource('carts', 'CartController');
Route::resource('transactions', 'TransactionController');
