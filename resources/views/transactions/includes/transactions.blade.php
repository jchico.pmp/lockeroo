<div class="col-12 px-0">
			{{-- TABLE START --}}
			<div class="table-responsive px-0">

				{{-- transaction table start --}}
				<table class="table table-striped watermarked">
					<tbody>
						<tr>
							{{-- username --}}
							<td>Customer Name:</td>
							<td>{{$transaction->user->name}}</td>
						</tr>
							{{-- transaction code --}}
						<tr>
							<td>Transaction Code:</td>
							<td>{{$transaction->transaction_code}}</td>
						</tr>
						{{-- payment_mode --}}
						<tr>
							<td>Payment Mode:</td>
							<td>{{$transaction->payment_mode->name}}</td>
						</tr>
						{{-- date of purchase --}}
						<tr>
							<td>Request Date:</td>
							<td>{{$transaction->created_at->format('F d, Y')}}</td>
						</tr>
						{{-- status --}}
						<tr>
							<td>Status:</td>
							
							@can('isAdmin')
							<td>{{-- {{$transaction->status->name}} --}}
											
											<form class="row" action="{{ route('transactions.update', ['transaction'=>$transaction->id])}}" method="POST">
												@csrf
												@method('PUT')

												<div class="col-8">
													
											<select name="status" id="" class="form form-control">
												@foreach($statuses as $status)
												<option value="{{$status->id}}" 
													{{$transaction->status_id == $status->id ? "selected" : ""}}
													>
													{{$status->name}}
												</option>
												@endforeach
											</select>
												</div>

												<div class="col-4">
													
												<input class="reg-icon-mid px-0" type="image" src="http://3.bp.blogspot.com/-RjuqUMNbPoA/XiGIIDrDJhI/AAAAAAAAL1U/Mfn8wkGMxvM6wF4bzHp3WkeyGYwTxW7bgCK4BGAYYCw/s400/icons8-edit-100.png">
												</div>
												</form>
												

											
							</td>
							@endcan

							@can('isUser')
							<td>
								{{$transaction->status->name}}
							</td>
							@endcan

						</tr>
					</tbody>
					{{-- transaction table end --}}
				</table>

				

			</div>
			{{-- TABLE END --}}
		</div>