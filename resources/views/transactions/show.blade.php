@extends('layouts.app')

@section('content')

	<div class="container lockeroo">
		<div class="row">
			<div class="col-12 card-header">
				<h3 class="text-center">
					Transaction
				</h3>
				<hr>
			</div>
		</div>

		@include('transactions.includes.transactions')

<div class="table-responsive">
		{{-- product_transaction table start --}}
				<table class="table table-striped table-hover">
					<thead>
						<th scope="row">Product Name</th>
						<th scope="row">Location</th>
						<th scope="row">Price</th>
						<th scope="row">Days</th>
						<th scope="row">Subtotal</th>
						
						@can('isAdmin')
						<th scope="row">ID</th>
						<th scope="row">Item Code</th>
						<th scope="row">Locker Status</th>
					@endcan
					</thead>

					<tbody>
						{{-- product_transaction details start --}}
							
						@foreach($transaction->products as $transaction_product)
							<tr>
								<td>
									{{$transaction_product->name}}
								</td>
								<td>
									{{$transaction_product->location->name}}
								</td>
								<td>
									&#8369; {{ number_format($transaction_product->pivot->price,2)}}
								</td>
								<td>
									{{$transaction_product->pivot->duration}}
								</td>
								<td>
									&#8369; {{ number_format($transaction_product->pivot->subtotal,2)}}
								</td>

								@can('isAdmin')
								<td>
									{{$transaction_product->id}}
								</td>
								<td>
									
										<form class="row" action=" {{ route('transactions.update', ['transaction'=>$transaction->id])}}" method="POST">
												@csrf
												@method('PUT')

												<input type="hidden" name="product_id" value="{{$transaction_product->id}}" >

												<div class="col-8">
											<select name="unique_code" id="unique_code" class="form form-control">
												
												
												@foreach($basic_lockers as $bs)
												@if($bs->product_id  === $transaction_product->id)
												<option value="{{$bs->unique_code}}" 
													{{-- {{$transaction->status_id == $status->id ? "selected" : ""}} --}}
													>
													{{$bs->unique_code}}
													
												</option>

												@endif
												@endforeach

											

											</select>
											</div>

											<div class="col-4">
												<input class="reg-icon-mid px-0" type="image" src="http://3.bp.blogspot.com/-RjuqUMNbPoA/XiGIIDrDJhI/AAAAAAAAL1U/Mfn8wkGMxvM6wF4bzHp3WkeyGYwTxW7bgCK4BGAYYCw/s400/icons8-edit-100.png">
												</div>

												</form>

								</td>
								<td>
									
											<form class="row" action="{{ route('transactions.update_ps', ['transaction'=>$transaction->id])}}" method="POST">
												@csrf
												@method('PUT')

												<div class="col-8">
											<select name="ps_update" id="ps_update" class="form form-control">
												@foreach($product_statuses as $ps)
												<option value="{{$ps->name}}" 
													{{-- {{$transaction->status_id == $status->id ? "selected" : ""}} --}}
													>
													{{$ps->name}}
												</option>
												@endforeach
											</select>
											</div>

											<div class="col-4">
												<input class="reg-icon-mid px-0" type="image" src="http://3.bp.blogspot.com/-RjuqUMNbPoA/XiGIIDrDJhI/AAAAAAAAL1U/Mfn8wkGMxvM6wF4bzHp3WkeyGYwTxW7bgCK4BGAYYCw/s400/icons8-edit-100.png">
												</div>
												
												</form>



								</td>
									@endcan

							</tr>

							@endforeach
						{{-- product_transaction details start --}}


					</tbody>
					<tfoot>
						<td class="text-right" colspan="4"><strong>Total</strong></td>
						<td >&#8369; {{ number_format($transaction->total,2)}}</td>
						<td></td>
						<td></td>
						
					</tfoot>
				</table>

				{{-- product_transaction table start --}}
		
	</div>
	</div>

@endsection