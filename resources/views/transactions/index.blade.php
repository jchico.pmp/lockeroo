@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2 card-header px-0">
			<h3 class="text-center">All Transactions</h3>
		</div>
	</div>
</div>

<div class="accordion col-12 col-md-8 offset-md-2 px-0 card-body lockeroo" id="accordionExample">
  
	@foreach($transaction as $transaction)
  <div class="card container lockeroo">
    <div class="row">
    <div class="text-light col-8 card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link " type="button" data-toggle="collapse" data-target="#collapse-{{$transaction->id}}" aria-expanded="true" aria-controls="collapse-{{$transaction->id}}">
          Transaction</span> ID: {{$transaction->id}}
          <sup>

            @if($transaction->status->id == "2")
<span class="badge badge-warning">
{{$transaction->status->name}}
</span>
@elseif($transaction->status->id == "3")
<span class="badge badge-danger">
{{$transaction->status->name}}
</span>
@else
<span class="badge badge-secondary">
{{$transaction->status->name}}
</span>
@endif

          </sup>
        </button>
      </h2>
      </div>

      <div class="text-light text-right bg-secondary mr-auto col-4 card-header">Total Price : &#8369; {{ number_format($transaction->total,2)}}</div>
    </div>

    <div id="collapse-{{$transaction->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        
		@include('transactions.includes.transactions')
		<div class="row">
			<div class="col text-center pt-2">
		<a href="/transactions/{{$transaction->id}}" class="btn btn-lockblue">
  	View Details
  </a>
  </div>
  </div>
		



      </div>
    </div>
  </div>
 
  @endforeach
  </div>
</div>

@endsection