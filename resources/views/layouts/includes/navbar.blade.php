<nav class="navbar navbar-expand-md navbar-dark bg-lockblue shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                  <img class="logo-image" src="https://1.bp.blogspot.com/-qzR6NUx4jFs/XhnU8Jt9HII/AAAAAAAALx8/X8N22rxI3q4U7-ClvTzuqlGjOXrh_kX1gCLcBGAsYHQ/s1600/lockeroo-logo-transparent-less.png" alt="profile Pic">
                    <!-- {{ config('app.name', 'Lockeroo') }} -->
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item">
                        <a class="nav-link" href="{{route('products.index')}}">Lockers</a>
                      </li>
                     
                     @can('isAdmin')
                      <li class="nav-item">
                        <a class="nav-link" href="{{route('products.create')}}">Add  Lockers</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="{{route('locations.index')}}">Add  Location</a>
                      </li>
                      @endcan
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        @can('isUser')
                        <li class="nav-item">
                        <a class="nav-link" href="{{ route('carts.index') }}"><img class="cart-image" src="https://1.bp.blogspot.com/-J-UFZaS5cRk/XhnpHXkzHSI/AAAAAAAALyg/IZcXJHM2BpAg19drShryfQmc3AgH8pt0ACLcBGAsYHQ/s1600/icons8-shopping-cart-64.png" alt=""> Cart <span class="badge badge-primary">{{Session::has('cart') ? count(Session::get('cart')) : "0"}}</span></a>

                      </li>
                      @endcan
                      @can('isLogged')
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('transactions.index') }}">Transactions</a>
                      </li>
                      @endcan
                      
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                    <img class="reg-icon" src="{{ asset('/login.png')}}" alt=""> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>