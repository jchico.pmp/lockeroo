@extends('layouts.app')

@section('content')

 {{-- {{ dd(Session::get('cart')) }} --}}
	<div class="container">
		<div class="row card-header">



			<div class="col-10">
				<h3>My Cart <img class="reg-icon" src="https://1.bp.blogspot.com/-J-UFZaS5cRk/XhnpHXkzHSI/AAAAAAAALyg/IZcXJHM2BpAg19drShryfQmc3AgH8pt0ACLcBGAsYHQ/s1600/icons8-shopping-cart-64.png" alt="">
					@if(Session::has('status')) 
						
							<sup>
							<span class="badge badge-danger">
							{{Session::get('status')}}
						</span>
							</sup>
						@endif


				</h3>
			</div>

			
					
					<div class="col-2 text-right px-0">
						<form action="{{ route('carts.empty')}}" method="POST">
					@csrf
					@method('DELETE')
				<input class="reg-icon-mid" type="image" src="https://1.bp.blogspot.com/-0XyjjEBeaqg/XiQjSKLaXRI/AAAAAAAAL2I/0MSDv_MexOUlKzc2ByAgXLFvHJQaFOg0gCLcBGAsYHQ/s1600/DELETE-ALL.png">

				</form>
					</div>
				
			
			{{-- @if(Session::has('status'))
					<div class="col-12">
						<div class="alert alert-warning">
							{{ Session::get('status') }}
						</div>
					</div>
			@endif --}}
		</div>


			@if(Session::has('cart'))
			<div class="row">
			<div class="col-12">

				{{-- TABLE STARTS HERE --}}
				<div class="table-responsive mx-0 px-0">
					<table class="table table-striped table-hover text-center mx-0 px-0">
						<thead  class="lockeroo">
							<th scope="col">Name</th>
							<th scope="col">Location</th>
							<th scope="col">Size</th>
							<th scope="col">Rental Fee Per Diem</th>
							<th scope="col">Days</th>
							
							<th scope="col">Subtotal</th>
							<th scope="col">Action</th>
						</thead>

						<tbody class="watermarked">
							@foreach($products as $product)
							{{-- START OF ROW --}}
								<tr>
									{{-- name --}}
									<th scope="row">
									{{$product->name}}
								</th>
								{{-- location --}}
								<td>
										{{$product->location->name}}
									</td>
									{{-- size --}}
								<td>
										{{-- {{$product->size->name}} --}}
										{{$product->acronym}}
									</td>

									{{-- price --}}
									<td>
										&#8369; <span>{{ number_format($product->price,2) }}</span>
									</td>

									
									{{-- duration --}}
									<td class="text-center">
										<div class="add-tocart-field mb-1">
											<form action="{{ route('carts.update',['cart' => $product->id])}}" method="POST">
												@csrf
												@method('PUT')

												<div class="row">
													
													<div class="col-12 col-md-3">
														<input class="reg-icon-mid px-0" type="image" src="http://3.bp.blogspot.com/-RjuqUMNbPoA/XiGIIDrDJhI/AAAAAAAAL1U/Mfn8wkGMxvM6wF4bzHp3WkeyGYwTxW7bgCK4BGAYYCw/s400/icons8-edit-100.png">
													</div>

													<div class="col-12 col-md-9">
														<input type="number" name="duration" id="duration" class="form-control mb-2 w-100 " value="{{$product->duration}}" min="1">
													</div>
												</div>
												
												

												{{-- <button class="btn btn-outline-secondary w-100" type="submit">Edit</button> --}}
											</form>
										</div>
									</td>
									{{-- subtotal --}}
									<td>
										&#8369; <span>{{number_format($product->subtotal,2)}}</span>
									</td>
									{{-- action --}}
									<td>
										<form action="{{ route('carts.destroy', ['cart'=> $product->id])}}" method="POST">
											@csrf
											@method('DELETE')
											<input class="reg-icon-mid" type="image" src="http://3.bp.blogspot.com/-m8Vm-8GTWsY/XiGIILXPooI/AAAAAAAAL1Y/_7v6C97VGbUYwnW6OvOvt7zEn4f1GLWhwCK4BGAYYCw/s400/icons8-delete-100.png">
										</form>
									</td>
								</tr>
							{{-- END OF ROW --}}
							
							@endforeach

						</tbody>

						<tfoot>
							<tr>
								<td colspan="5" class="text-right bg-secondary text-light">
								Total
							</td>
							<td class="bg-secondary text-light">
										&#8369; <span id="total">{{number_format($total,2)}}</span>
							</td>
							{{-- action --}}
									<td class="bg-secondary">
										
										{{-- @can('isLogged') --}}
										<form action="{{ route('transactions.store') }}" method="POST">
											@csrf
											<button class="btn btn-primary w-100 mb-3">
												Checkout
											</button>
										</form>
										<div id="paypal-btn">
											
										</div>

										{{-- @endcan --}}
										{{-- @cannot('isLogged') --}}
											<a href="{{ route('login') }}" class="btn btn-success w-100">Please Login to Continue</a>
										{{-- @endcannot --}}

									</td>
							</tr>
						</tfoot>
					</table>
					{{-- END OF TABLE --}}
				{{-- <form action="{{ route('carts.empty')}}" method="POST">
					@csrf
					@method('DELETE')
				<button type="submit" class="btn btn-outline-danger">
												Clear Cart
											</button>
				</form>							 --}}
				</div>
			</div>
			@else
					<div class="col-12 text-center">
						<img class="empty-cart" src="https://1.bp.blogspot.com/-iOnnH7qrdCA/XiQfSpzkW3I/AAAAAAAAL18/0RAEMNPoqqs3xOrQC9r530fKUqD-TVvSwCLcBGAsYHQ/s1600/empty-cart-2.png" alt="">
					</div>
			@endif
		</div>
	</div>

{{-- <script src="https://www.paypal.com/sdk/js?client-id=AaR7pFBDf38hOkFkwVWOip418KchDWejh2Mj7CobbcgWx8G_xAVKdMGrKDnO1bw8BwAW23uSVOEySnXn"></script>
<script>
	paypal.Buttons({
	createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: {{ isset($total) ? $total : "0" }}
          }
        }]
      });
    },

    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        alert('Transaction completed by ' + data.orderID);

        let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        let data = { transactionCode : data.orderID };

        fetch("{{ route('transactions.paypal')}}", {
        		method: 'post',
        		body: JSON.stringify(data),
        		headers: {'X-CSRF-TOKEN': csrfToken}
        	})
        .then(response =>response.json())
        .then(res =>console.log(res));

        });

      }
    
}).render('#paypal-btn');
</script> --}}
@endsection