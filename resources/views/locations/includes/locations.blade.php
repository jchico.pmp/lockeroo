
<main class="container-fluid watermarked">
	<div class="row">
		<div class="col-12 col-md-10 pr-0">
			<form class="form-group" action="{{ route('locations.update', ['location'=> $location->id])}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PUT')


<!-- location name -->
<div class="form-group row">

<div class="col-12 col-md-1 offset-md-1 px-1">
	{{-- 	<button type="submit" class="btn btn-lockwhite w-100 mb-1 mx-auto">Update</button> --}}
<input class="reg-icon-mid" type="image" src="http://3.bp.blogspot.com/-RjuqUMNbPoA/XiGIIDrDJhI/AAAAAAAAL1U/Mfn8wkGMxvM6wF4bzHp3WkeyGYwTxW7bgCK4BGAYYCw/s400/icons8-edit-100.png">

	</div>

	<div class="col-12 col-md-10 pl-3">
		<input type="text" name="edit-name" class="form-control mb-2" placeholder="Location Name" value="{{ $location->name}}">

		

	</div>


	
</div>


</form>
		</div>
		<div class="col-12 col-md-2  ml-auto">
			<form class="" action="{{ route('locations.destroy', ['location'=>$location->id])}}" method="POST">
	@csrf
	@method('DELETE')
	<div>
		{{-- <button class="btn btn-lockred w-100 mb-2">Delete</button> --}}
		<input class="reg-icon-mid" type="image" src="http://3.bp.blogspot.com/-m8Vm-8GTWsY/XiGIILXPooI/AAAAAAAAL1Y/_7v6C97VGbUYwnW6OvOvt7zEn4f1GLWhwCK4BGAYYCw/s400/icons8-delete-100.png">
		</div>
</form>
		</div>

	</div>
</main>




