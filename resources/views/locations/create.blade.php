@extends('layouts.app')

@section('content')

<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="card lockeroo">
					<div class="card-header">
				<h4>Add New Location</h4>
				</div>
				
				<div class="card-body watermarked">
				<form class="form-group" action="{{ route('locations.store')}}" method="POST" enctype="multipart/form-data">
					@csrf

					{{-- @if($errors->has('name'))
						<div class="alert alert-danger">
							{{$errors->first('name')}}
						</div>
					@endif --}}

					
					<!-- locker name -->
					<div class="form-group row">

						<label for="name" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="http://4.bp.blogspot.com/-4MfDLOiAssY/Xh3DZLngjAI/AAAAAAAAL0U/FX8puGiPctY8DQ8eV6SbjZ6BISpWHII7gCK4BGAYYCw/s400/icons8-location-64.png" alt="">
       <!-- {{ __('Name') }} -->
      </label>
					
					<div class="col-md-9">
					<input type="text" name="name" id="name" class="form-control mb-2 @error('name') is-invalid @enderror" placeholder="Name of Location" value="{{ old('name')}}">

					@error('name')
       <span class="invalid-feedback" role="alert">
           <strong>{{ $message }}</strong>
       </span>
   @enderror

					</div>
					
					
				</div>


					
					<div class="form-group row text-left"><button type="submit" class="btn btn-lockred mb-2 mx-auto">Add Location</button>
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	</div>

@endsection