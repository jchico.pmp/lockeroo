@extends('layouts.app')

@section('content')

<div class="container">
			
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="card lockeroo">
					<div class="card-header">
						<h4>Location Data Sheet
				<sup>
							
							@if($errors->has('name'))
						<span class="badge badge-danger">
							{{$errors->first('name')}}
						</span>
					@endif
							</sup>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>

	


<div class="container">
			
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="card lockeroo">
					<div class="card-body">
				<h4>Add New Location
				
						</h4>
				</div>
				
				<div class="card-body watermarked">
				<form class="form-group" action="{{ route('locations.store')}}" method="POST" enctype="multipart/form-data">
					@csrf

					

					
					<!-- locker name -->
					<div class="form-group row">

						<label for="name" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="http://4.bp.blogspot.com/-4MfDLOiAssY/Xh3DZLngjAI/AAAAAAAAL0U/FX8puGiPctY8DQ8eV6SbjZ6BISpWHII7gCK4BGAYYCw/s400/icons8-location-64.png" alt="">
       <!-- {{ __('Name') }} -->
      </label>
					
					<div class="col-12 col-md-8">
					<input type="text" name="name" class="form-control mb-2 " placeholder="Name of Location" value="{{ old('name')}}">

					

					</div>

					<div class="col-12 col-md-2">
						{{-- <button type="submit" class="btn btn-lockred mb-2 mx-auto"><img class="reg-icon" src="http://4.bp.blogspot.com/-QyNrNIQFx0A/XiGIH7SdH1I/AAAAAAAAL1I/vEbvN9_D4worChlDUzaC41LDYzAtH5kJACK4BGAYYCw/s400/icons8-add-100.png" alt=""></button> --}}
						<input class="reg-icon-mid" type="image" src="http://4.bp.blogspot.com/-7GLXVivFZDs/XiGLAJiQkYI/AAAAAAAAL1s/apOMZkaWVUgZJUVuxUMxdYJCrzRJQUAFACK4BGAYYCw/s400/icons8-add-64.png">
					</div>
					
					
				</div>


					
					
						
				</form>
				</div>
			</div>
		</div>
	</div>
	</div>

	

	{{-- START OF LOCATIONS --}}
		<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<div class="card lockeroo">
					<div class="card-body">
				<h4>Current Locations <sup>@if(Session::has('status'))
		<div class="badge badge-danger mb-3 mx-auto text-center">
			{{Session::get('status')}}
			
		</div>

	@endif</sup></h4>
				
				</div>
		<div class="card-body watermarked">
			@foreach($locations as $location)
			@include('locations.includes.locations')
			@endforeach
		</div>
				
			</div>
		</div>
	</div>
		{{-- END OF LOCATIONS --}}

@endsection