@extends('layouts.app')

@section('content')


{{-- TABLE ADMIN VIEW --}}
@can('isAdmin')
<div class="container">
	<div class="row">
		<div class="col-12 col-md-10 offset-md-1 card-header lockeroo">
			<h1>Available Stocks</h1>
		</div>
		<div class="table-responsive col-12 col-md-10 offset-md-1">
		{{-- product_transaction table start --}}
				<table class="table table-striped table-hover">
					<thead class="card-header lockeroo">
						<th scope="row">Item Code</th>
						{{-- <th scope="row">Model</th>
						<th scope="row">Size</th>
						<th scope="row">Location</th> --}}
						<th scope="row">Status</th>
					</thead>

					<tbody class="card-body">
						{{-- product_transaction details start --}}
							
						@foreach($lockers as $locker)
							<tr>
								<td>
									{{$locker->unique_code}}
								</td>
								{{-- <td>
									{{$locker->products->name}}
								</td>
								<td>
									{{$products->size->name}}
								</td>
								<td>
									{{$product->location->name}}
								</td> --}}
								<td>
									{{$locker->product_status->name}}
								</td>
							</tr>

							@endforeach
						{{-- product_transaction details start --}}


					</tbody>
					<tfoot>
						

					</tfoot>
				</table>

				{{-- product_transaction table start --}}
		
	</div>
	</div>
</div>
@endcan


{{-- PRODUCTS VIEW --}}

<div class="container">
	<div class="row">
		<div class="col-12">
			<form action="" method="get">
				
				{{-- <div class="row">
					<div class="col">
						<select name="category" id="category" class="form-control">
							<option value="">All</option>
							@foreach($sizes as $size)
							<option value="{{$size->id}}">{{$size->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col">
				<button class="btn btn-outline-primary">
					Filter
				</button>
						
					</div>
				</div> --}}
				
			</form>
		</div>
	</div>
	@if(Session::has('status'))
		<div class="badge badge-danger mb-3 mx-auto text-center">
			{{Session::get('status')}}
		</div>

	@endif

	{{-- @include('products.includes.error-status') --}}
	<div class="row">


	@foreach($products as $product)
		{{-- START OF CARDS --}}
		<div class="col-12 col-md-4 col-lg-3 pb-3">
			@include('products.includes.products')
		</div>
		{{-- END OF CARD --}}
	@endforeach
	</div>
</div>

@endsection