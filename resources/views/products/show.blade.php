@extends('layouts.app')

@section('content')

<div class="col-12 px-3 text-right card-body"><h6 class="btn btn-lockwhite"><a href="{{ route('products.index')}}">Back to Products</a></h6>
	</div>

{{-- @include('products.includes.error-status') --}}

{{-- START OF CARDS --}}

	<div class="container">
		<div class="row">
			

			<div class="col-12 col-md-6 pb-3">
			<div class="card">
				<img src="/public/{{$product->image}}" alt="" class="card-img-top">
				
				
			</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="card-body">
					<div class="card-header lockeroo">
					<h5 class="">{{$product->name}}</h5>
					</div>
					<p class="card-text">&#8369;{{number_format($product->price,2)}}</p>
					<p class="card-text"><small class="badge badge-primary">{{$product->size->name}}</small></p>
					<p class="card-text">{{$product->description}}</p>
				</div>
			<div class="card-footer">
					{{-- @cannot('isAdmin') --}}
					<form action="{{ route('carts.update',['cart' => $product->id]) }}" method="POST">
						@csrf
						@method('PUT')
						
						<input type="number" name="duration" id="duration" class="form-control mb-2" min="1">
						
						<button class="btn btn-lockblue w-100 mb-2">Add to Cart</button>
						
					</form>
					{{-- @endcannot --}}
					<a href="{{ route('products.show', ['product'=> $product->id])}}" class="btn btn-lockwhite w-100 mb-2">View Locker</a>
					
					{{-- @can('isAdmin') --}}
					<a href="/products/{{$product->id}}/edit" class="btn btn-lockblue w-100 mb-2">Edit Locker</a>
					<form action="{{ route('products.destroy', ['product'=>$product->id])}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-lockred w-100 mb-2">Delete Locker</button>
					</form>
					{{-- @endcan --}}
				</div>
		</div>

		</div>
	</div>
		
		{{-- END OF CARD --}}



@endsection