@extends('layouts.app')

@section('content')

<div class="container">
		<div class="row">

			<div class="col-12 col-md-4 mx-auto">
				@include('products.includes.products')

			</div>
			<div class="col-12 col-md-8 mx-auto">
				<div class="card lockeroo">
					<div class="card-header">
				<h4>Edit Locker Sheet
					@if(Session::has('status')) 
						
							<sup>
							<span class="badge badge-success">
							{{Session::get('status')}}
							</sup>
						</span>
						
				</h4>


					@endif
				</div>
				
				<div class="card-body watermarked">
				<form class="form-group" action="{{ route('products.update', ['product'=> $product->id])}}" method="POST" enctype="multipart/form-data">
					@csrf
					@method('PUT')

					{{-- @if($errors->has('name'))
						<div class="alert alert-danger">
							{{$errors->first('name')}}
						</div>
					@endif --}}

					
					<!-- locker name -->
					<div class="form-group row">

						<label for="name" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="https://1.bp.blogspot.com/-ALf_hY4lWUU/Xh3Cy-ZyyGI/AAAAAAAALz4/8gt9ImeHRgYrycTFT_Gus_Wh98CfiyJ-ACLcBGAsYHQ/s1600/icons8-safe-64.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<input type="text" name="name" id="name" class="form-control mb-2 @error('name') is-invalid @enderror" placeholder="Locker Name" value="{{ $product->name}}">

					@error('name')
       <span class="invalid-feedback" role="alert">
           <strong>{{ $message }}</strong>
       </span>
   @enderror

					</div>
					
					
				</div>


					
					<!-- locker price -->
					<div class="form-group row">

						<label for="price" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="http://4.bp.blogspot.com/-cu1R8d4-Zas/Xh3E5A8I_vI/AAAAAAAAL0o/GD-37mLgnH4a7Aa-APw6S7Nn0rTQDvX6wCK4BGAYYCw/s400/icons8-us-dollar-64.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<input type="text" name="price" id="price" class="form-control mb-2 @error('price') is-invalid @enderror" placeholder="Locker Rental Price per Day" value="{{ $product->price}}">

					@error('price')
       <span class="invalid-feedback" role="alert">
           <strong>{{ $message }}</strong>
       </span>
   @enderror
				</div>
			</div>

			<!-- locker location -->
						<div class="form-group row">

						<label for="location-id" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="http://4.bp.blogspot.com/-4MfDLOiAssY/Xh3DZLngjAI/AAAAAAAAL0U/FX8puGiPctY8DQ8eV6SbjZ6BISpWHII7gCK4BGAYYCw/s400/icons8-location-64.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<select name="location-id" id="location-id" class="custom-select">
						@foreach($locations as $location)
							<option 
								value="{{$location->id}}"
								{{ $product->location_id == $location->id ? "selected" : ""}}
								>
								{{$location->name}}
							</option>
						@endforeach	
					</select>
				</div>
			</div>
					
				{{-- 	@if($errors->has('image'))
						<div class="alert alert-danger">
							{{$errors->first('image')}}
						</div>
					@endif --}}
					
					<!-- locker size -->
						<div class="form-group row">

						<label for="size-id" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="https://1.bp.blogspot.com/-Na9k-xcN1mg/Xh3Cyu6NSFI/AAAAAAAALz0/WliDCNbK7awS85AH1ER--UyDqeCJl3MggCLcBGAsYHQ/s1600/icons8-open-box-64.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<select name="size-id" id="size-id" class="custom-select">
						@foreach($sizes as $size)
							<option 
								value="{{$size->id}}"
								{{ $product->size_id == $size->id ? "selected" : ""}}
								>
								{{$size->name}}
							</option>
						@endforeach	
					</select>
				</div>
			</div>
					
					{{-- @if($errors->has('image'))
						<div class="alert alert-danger">
							{{$errors->first('image')}}
						</div>
					@endif --}}
					
					<!-- locker image -->
					<div class="form-group row">

						<label for="image" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="http://4.bp.blogspot.com/-ZZoTgQcCz-o/Xh3E5DYoQQI/AAAAAAAAL0s/ZkwnPFriJM8dnWKl0E1yUfSoUcVSDo8igCK4BGAYYCw/s400/icons8-photo-gallery-64.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<input type="file" name="image" id="image" class="form-control-file mb-2">
					
					</div>
				</div>

					
					{{-- @if($errors->has('description'))
						<div class="alert alert-danger">
							{{$errors->first('description')}}
						</div>
					@endif --}}
					
					<!-- locker description -->
					<div class="form-group row">

						<label for="description" class="col-md-2 col-form-label text-md-right">
       <img class="reg-icon" src="https://1.bp.blogspot.com/-lt_jfQcesik/Xh3CxgNtB5I/AAAAAAAALzs/RuL1GGe2rmQQi2pEVQiBFZQBwwO8EaN4QCLcBGAsYHQ/s1600/icons8-news-256.png" alt="">
       
      </label>
					
					<div class="col-md-9">
					<textarea name="description" id="description" cols="10" rows="10" class="form-control mb-2 @error('description') is-invalid @enderror" placeholder="Locker Description">{{ $product->description}}</textarea>

					@error('description')
       <span class="invalid-feedback" role="alert">
           <strong>{{ $message }}</strong>
       </span>
   @enderror
				</div>
			</div>

					<div class="form-group row text-left"><button type="submit" class="btn btn-lockred mb-2 mx-auto">Update Locker Details</button>
						</div>
				</form>
				</div>
			</div>
		</div>
	</div>
	</div>

	
							
@endsection