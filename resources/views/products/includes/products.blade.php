<div class="card">
				<img src="/public/{{$product->image}}" alt="" class="card-img-top">
				<div class="card-body">
					<h5 class="card-title">{{$product->name}}</h5>
					<p class="card-text">&#8369;{{number_format($product->price,2)}}</p>
					<p class="card-text"><small class="badge badge-primary">{{$product->size->name}}</small></p>
					<p class="card-text">{{$product->description}}</p>
				</div>
				<div class="card-footer">
					@cannot('isAdmin')
					<form action="{{ route('carts.update',['cart' => $product->id]) }}" method="POST">
						@csrf
						@method('PUT')
						
						<input type="number" name="duration" id="duration" class="form-control mb-2" min="1">
						
						<button class="btn btn-lockblue w-100 mb-2">Add to Cart</button>
						
					</form>
					@endcannot
					<a href="{{ route('products.show', ['product'=> $product->id])}}" class="btn btn-lockwhite w-100 mb-2">View Locker</a>
					
					@can('isAdmin')
					<a href="/products/{{$product->id}}/edit" class="btn btn-lockblue w-100 mb-2">Edit Locker</a>
					<form action="{{ route('products.destroy', ['product'=>$product->id])}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-lockred w-100 mb-2">Delete Locker</button>
					</form>
					@endcan
				</div>
			</div>