<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lockeroo</title>

        <!-- fav icon -->
        <link rel="icon" type="image/gif/png" href="{{asset('lockeroo-favicon.png')}}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">


        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- external css -->
        <link rel="stylesheet" href="{{asset('css/style.css')}}">
    </head>
    <body>
        <div class="flex-center container-fluid full-height">
            <!-- @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif -->

            <div class="content">
                <div class="row m-b-md">
                  <div class="col-12 col-md-8 offset-md-2">
                    <img class="main-logo" src="https://1.bp.blogspot.com/-iwqrWNUsIPY/Xhc5f6YIpmI/AAAAAAAALxw/xDVJi8co3ngQrnWdRiO7wGBkVUfJesh0wCLcBGAsYHQ/s1600/lockeroo-logo-transparen-2t.png" alt="">
                    </div>
                </div>
                <div class="row mb-3">
                  <div class="col-12 col-md-8 offset-md-2">
                    <span class="slogan mb-3 px-3">Your valuables—safe and secured. Lockers conveniently located in Metro Manila.</span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-12 col-md-6 offset-md-3">
                    
                    <div class="row">
                      <div class="col-12 col-md-6 left">
                    <a href="{{ route('register') }}" id="" class="btn btn-lockwhite mr-1 mb-3 w-75">Register</a>
                    </div>
                    <div class="col-12 col-md-6 right">
                    <a href="{{ route('login') }}" id="" class="btn btn-lockwhite mr-1 mb-3 w-75 ">Sign In</a>
                  </div>
                  </div>
                  </div>
                </div>

                <!-- <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div> -->
            </div>
        </div>
    

<!-- JS Popper jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    </body>
</html>
