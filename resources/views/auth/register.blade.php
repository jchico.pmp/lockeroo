@extends('layouts.reg')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card lockeroo">
                <div class="card-header">
                <h4>{{ __('Register') }}</h4>
               </div>

                <div class="card-body watermarked">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                            <img class="reg-icon" src="http://4.bp.blogspot.com/-U5hNbvP_dt0/XhqmnysoTQI/AAAAAAAALyw/Zclq2VfXcH0krczNTSoBkJ3LX8bG9yMDwCK4BGAYYCw/s400/icons8-name-80.png" alt="">
                            <!-- {{ __('Name') }} --></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">
                              <img class="reg-icon" src="http://4.bp.blogspot.com/-8upbryGUOJU/XhqpgZeiOCI/AAAAAAAALy8/QsOrO-7OnVgzfoxq_v8GriDn5WQVWlWjwCK4BGAYYCw/s400/icons8-laptop-e-mail-96.png" alt=""><!-- {{ __('E-Mail Address') }} --></label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail Address">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">
                            <img class="reg-icon" src="http://2.bp.blogspot.com/-7v2PSWBq5dI/XhqqK7OOe3I/AAAAAAAALzM/AtAGnn8aDKgAV2mOgJMNtoSrKwx7aTGtwCK4BGAYYCw/s400/icons8-password-80.png" alt=""><!-- {{ __('Password') }} --></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">
                            <img class="reg-icon" src="http://3.bp.blogspot.com/-Kdo_2P77YHU/XhqqKwq_EQI/AAAAAAAALzQ/DYYQjED3_8Ybtu4rKi8jY5NqR5T3EGEZQCK4BGAYYCw/s400/icons8-password-reset-80.png" alt=""><!-- {{ __('Confirm Password') }} --></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-lockblue">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
