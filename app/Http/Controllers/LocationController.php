<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Location $location)
    {
        $this->authorize('create', $location);
        $locations = Location::all();
        return view('locations.index')->with('locations',$locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Location $location)
    {
         $this->authorize('create', $location);

        return view('locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $this->authorize('create', $location);

        $request->validate([
                    'name' => 'required|string',
                    ]);

        $location = new Location;

        $location->name = $request->input('name');
        $location->save();

        return redirect( route('locations.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
                $this->authorize('create', $location);

        return view('locations.show')->with('location', $location);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
                $this->authorize('create', $location);

        $request->validate([
                    'name' => 'required|string',
                    ]);

        $location->name = $request->input('name');
        $location->save();

        return redirect( route('locations.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
                $this->authorize('create', $location);

        $location->delete();
        return redirect( route('locations.index'))->with('status','A location is deleted. To add new location, fill up above form.');
    }
}
