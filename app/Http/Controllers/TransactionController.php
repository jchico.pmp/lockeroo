<?php

namespace App\Http\Controllers;

use App\Transaction;
Use App\Product;
Use App\Status;
Use App\User;
Use App\Basic_locker;
Use App\Metal_lockermb;
Use App\Product_status;
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        // dd($transaction->id);
        if(Auth::user()->role_id ==1){
            $transactions = Transaction::all();
        } else {
            $transactions = Transaction::all()->whereIn('user_id', Auth::user()->id);
        }
        return view('transactions.index')->with('transaction',$transactions)->with('statuses',Status::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->transaction_code = Auth::user()->id.Str::random(8);

        // $transaction->user_id = Auth::user()->id;
        // $transaction->transaction_code = Auth::user()->id.Str::random(8);


        $transaction->save();

        // dd($transaction->id);

        $product_ids = array_keys(Session::get('cart'));


        $products = Product::find($product_ids);





        $total = 0;
        foreach ($products as $product) {
            $product->duration = Session::get("cart.$product->id");
            $product->subtotal = $product->price * $product->duration;
            $total += $product->subtotal;


            $transaction->products()
            ->attach(
                $product->id,
                [
                 "duration" => $product->duration,
                    "subtotal" => $product->subtotal,
                    "price" => $product->price
                ]);
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget('cart');

        return redirect( route('transactions.show', ['transaction' => $transaction->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        // $this->authorize('create', $transaction);
        $statuses = Status::all();
        $product_statuses = Product_status::all();
        $basic_lockers = Basic_locker::all();
         
        //  foreach ($transaction->products as $tp) {
        
        // $basic_locker = Basic_locker::all()->whereIn('product_id', $tp->id);
        
        // // dd($basic_locker);
        //  };

        // $specific = Transaction::find($product_ids);

        return view('transactions.show')->with('transaction',$transaction)->with('statuses',$statuses)->with('basic_lockers',$basic_lockers)->with('product_statuses',$product_statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction, Basic_locker $basic_locker )
    {
        $transaction->status_id = $request->status;

        $unique = $request->input('unique_code');
        $product_id = $request->product_id;

        $transaction->products()->updateExistingPivot($product_id, ["unique_code" => $unique]);

        // $transaction->products()
        //     ->attach(
        //         $product_id,
        //         [
        //          "duration" => $product->duration,
        //             "subtotal" => $product->subtotal,
        //             "price" => $product->price,
        //             "unique_code" => $unique
        //         ]);



        $transaction->save();

        // $ps_update = $request->ps_update;
        // $basic_locker->name = $ps_update;

        

            // dd($unique);
        // $transactions = Transaction::all()->whereIn('user_id', Auth::user()->id);   

        


        return redirect( route('transactions.show',['transaction'=>$transaction->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function update_ps(Request $request, Transaction $transaction)
    {
        $ps_update = $request->ps_update;
        // dd($ps_update);

        return redirect( route('transactions.show',['transaction'=>$transaction->id]));
    }
}
