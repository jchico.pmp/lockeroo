<?php

namespace App\Http\Controllers;

use App\Metal_lockermb;
use Illuminate\Http\Request;

class MetalLockermbController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Metal_lockermb  $metal_lockermb
     * @return \Illuminate\Http\Response
     */
    public function show(Metal_lockermb $metal_lockermb)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Metal_lockermb  $metal_lockermb
     * @return \Illuminate\Http\Response
     */
    public function edit(Metal_lockermb $metal_lockermb)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Metal_lockermb  $metal_lockermb
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Metal_lockermb $metal_lockermb)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Metal_lockermb  $metal_lockermb
     * @return \Illuminate\Http\Response
     */
    public function destroy(Metal_lockermb $metal_lockermb)
    {
        //
    }
}
