<?php

namespace App\Http\Controllers;

use App\Product;
use App\Size;
use App\Location;
use App\Basic_locker;
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $sizes = Size::all();
        $locations = Location::all();
        $products = Product::all();
        $lockers = Basic_locker::all();
        

        return view('products.index')
        ->with('sizes',$sizes)
            ->with('locations',$locations)
            ->with('products',$products)
            ->with('lockers',$lockers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product)
    {
        $this->authorize('create', $product);

        $sizes = Size::all();
        $locations = Location::all();
        return view('products.create')
            ->with('sizes',$sizes)
            ->with('locations',$locations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $this->authorize('create', $product);
        $request->validate([
                    'name' => 'required|string',
                    'price' => 'required|numeric',
                    'location-id' => 'required',
                    'size-id' => 'required',
                    'description' => 'required|string',
                    'image' => 'required|image|max:5000'
                    ]);

        $product = new Product;

        

        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->size_id = $request->input('size-id');
        $product->location_id = $request->input('location-id');
        $product->description = $request->input('description');

        //get acronym of size
        $words = explode(" ", $product->size->name);
         $acronym = "";
         foreach ($words as $w) {
           $acronym .= $w[0];
         }
         


        $product->product_model = $acronym.'-'.$product->size_id.'-'.Str::random(5).'-00'.$product->location_id;

        $product->image = $request->image->store('public');
        $product->save();

        return redirect( route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $this->authorize('create', $product);
        $sizes = Size::all();
        $locations = Location::all();
        return view('products.edit')
            ->with('sizes',$sizes)
            ->with('locations',$locations)
            ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->authorize('create', $product);
        $request->validate([
                    'name' => 'required|string',
                    'price' => 'required|numeric',
                    'location-id' => 'required',
                    'size-id' => 'required',
                    'description' => 'required|string',
                    'image' => 'image|max:5000'
                    ]);

        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->size_id = $request->input('size-id');
        $product->location_id = $request->input('location-id');
        $product->description = $request->input('description');
        

        if($request->hasFile('image')) {
          $product->image = $request->image->store('public');
        } 

        

        
        $product->save();

        $request->session()->flash('status','Update to locker details successful!');

        return redirect( route('products.edit',['product'=>$product->id]));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->authorize('create', $product);
        $product->delete();
        return redirect( route('products.index'))->with('status','Locker is deleted. Go to Add Locker to add another locker product.');
    }
}
