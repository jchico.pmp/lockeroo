<?php

namespace App\Http\Controllers;

use App\Basic_locker;
use Illuminate\Http\Request;

class BasicLockerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Basic_locker  $basic_locker
     * @return \Illuminate\Http\Response
     */
    public function show(Basic_locker $basic_locker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Basic_locker  $basic_locker
     * @return \Illuminate\Http\Response
     */
    public function edit(Basic_locker $basic_locker)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basic_locker  $basic_locker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Basic_locker $basic_locker)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Basic_locker  $basic_locker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basic_locker $basic_locker)
    {
        //
    }
}
