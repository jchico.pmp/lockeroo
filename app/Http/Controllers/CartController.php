<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Location;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Location $location)
    {
        // $this->authorize('create', Session::get('cart'));
        if(Session::has('cart')) {
            //get all ids
        $product_ids = array_keys(Session::get('cart'));
        // query to the database
        // get all products that are in the session cart

        
        $products = Product::find($product_ids);

        $total = 0;

        foreach($products as $product) {
            $product->duration = Session::get("cart.$product->id");
            
            $product->subtotal = $product->duration * $product->price;
            $total += $product->subtotal;

        $words = explode(" ", $product->size->name);
         $acronym = "";

         foreach ($words as $w) {
           $acronym .= $w[0];
         }

         $product->acronym = $acronym;
            
        };


        // dd($products);
        return view('carts.index')->with('products', $products)->with('total',$total)->with('location', $location);
       } else {
            return view('carts.index');
       }    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cart)
    {
        $request->validate([
                  'duration'=>'required|min:1',
                  
                  ]);

        $qty = $request->duration;
        $duration = $request->input('duration');
        $request->session()
            ->put("cart.$cart", $qty);

        return redirect( route('carts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $cart)
    {
       $request->session()->forget("cart.$cart");

        if(count($request->session()->get('cart'))==0) {
            $request->session()->forget("cart");
        }

        return redirect( route('carts.index'))->with('status', 'One locker request to rent is removed.');
    }

    public function empty(){

        Session::forget('cart');
        return redirect(route('carts.index'))->with('status',"Cart is cleared.");
    }
}
