<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basic_locker extends Model
{
    public function product_status() {
    	return $this->belongsTo('App\Product_status');
    }

    public function products() {
    	return $this->belongsTo('App\Product');
    }
}
