<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

use App\Product;
use App\Policies\ProductPolicy;
use App\Policies\LocationPolicy;
use App\Policies\SizePolicy;
use App\Policies\TransactionPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Product::class => ProductPolicy::class,
        Location::class => LocationPolicy::class,
        Size::class => SizePolicy::class,
        Transaction::class => TransactionPolicy::class,
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user){
                return $user->role_id === 1;
        });

        Gate::define('isUser', function($user){
                return $user->role_id === 2;
        });

        Gate::define('isLogged', function($user){
                return $user !== null;
        });
    }
}
