<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_status extends Model
{
    public function basic_lockers() {
    	return $this->hasMany('App\Basic_locker');
    }

    public function metal_lockermbs() {
    	return $this->hasMany('App\Metal_lockermb');
    }
}
