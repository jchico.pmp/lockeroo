<?php

namespace App\Policies;

use App\Size;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SizePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any sizes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the size.
     *
     * @param  \App\User  $user
     * @param  \App\Size  $size
     * @return mixed
     */
    public function view(User $user, Size $size)
    {
        //
    }

    /**
     * Determine whether the user can create sizes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id === 1;
    }

    /**
     * Determine whether the user can update the size.
     *
     * @param  \App\User  $user
     * @param  \App\Size  $size
     * @return mixed
     */
    public function update(User $user, Size $size)
    {
        //
    }

    /**
     * Determine whether the user can delete the size.
     *
     * @param  \App\User  $user
     * @param  \App\Size  $size
     * @return mixed
     */
    public function delete(User $user, Size $size)
    {
        //
    }

    /**
     * Determine whether the user can restore the size.
     *
     * @param  \App\User  $user
     * @param  \App\Size  $size
     * @return mixed
     */
    public function restore(User $user, Size $size)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the size.
     *
     * @param  \App\User  $user
     * @param  \App\Size  $size
     * @return mixed
     */
    public function forceDelete(User $user, Size $size)
    {
        //
    }
}
