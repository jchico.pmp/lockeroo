<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function size() {
    	return $this->belongsTo('App\Size');
    }

    public function location() {
    	return $this->belongsTo('App\Location');
    }

    public function basic_lockers() {
    	return $this->belongsToMany('App\Basic_locker','basic_locker')
    	->withPivot('unique_code');
    }

    
    
}
