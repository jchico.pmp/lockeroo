<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
        		'name'=> 'Small Box'
        ]);
        DB::table('sizes')->insert([
        		'name'=> 'Medium Box'
        ]);
        DB::table('sizes')->insert([
        		'name'=> 'Big Box'
        ]);
        DB::table('sizes')->insert([
        		'name'=> 'Carry-on Luggage'
        ]);

        DB::table('sizes')->insert([
        		'name'=> 'Small Checked Luggage'
        ]);

        DB::table('sizes')->insert([
        		'name'=> 'Big Checked Luggage'
        ]);

        DB::table('sizes')->insert([
        		'name'=> 'Balikbayan Carton'
        ]);

        DB::table('sizes')->insert([
        		'name'=> 'Suitcase'
        ]);

        DB::table('sizes')->insert([
        		'name'=> 'Self-Storage Locker'
        ]);
    }
}
