<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            SizeSeeder::class,
            LocationSeeder::class,
            ProductStatusSeeder::class,
            ProductSeeder::class,
            BasicLockerSeeder::class,
            MetalLockermbSeeder::class,
            UserSeeder::class,
        	StatusSeeder::class,
        	PaymentModeSeeder::class
            
        ]);
    }
}
