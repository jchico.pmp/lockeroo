<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        		'name'=> 'Basic Locker SB',
        		'product_model'=>'BL-SB-01',
        		'price'=>'100',
        		'description'=> 'Basic Locker perfect for small envelopes.',
        		'size_id'=> 1,
        		'location_id' => 1,
                // 'product_status_id' => 1,
        		'image'=> 'public/yoW7MdER4pbLG3phxAUOT18c0uXJhSs9h6sc3BNR.jpeg'
        ]);

        DB::table('products')->insert([
                'name'=> 'Metal Locker MB',
                'product_model'=>'ML-MB-02',
                'price'=>'200',
                'description'=> 'Metal Locker perfect for short brown envelopes.',
                'size_id'=> 2,
                'location_id' => 1,
                // 'product_status_id' => 1,
                'image'=> 'public/VKkQeyX491RMlYW4j7O78c90UgYWFlTFGVzEriZ3.jpeg'
        ]);

        DB::table('products')->insert([
                'name'=> 'Basic Locker BB',
                'product_model'=>'BL-BB-01',
                'price'=>'300',
                'description'=> 'Basic Locker perfect for long brown envelopes.',
                'size_id'=> 3,
                'location_id' => 1,
                // 'product_status_id' => 1,
                'image'=> 'public/XnC9O4ZygoHfDIiKLTQkRcqtO0kReVms5YrwdOAt.jpeg'
        ]);

        DB::table('products')->insert([
                'name'=> 'Basic Locker CL',
                'product_model'=>'BL-CL-01',
                'price'=>'400',
                'description'=> 'Basic Locker perfect for laptops, tablets and long brown envelopes.',
                'size_id'=> 4,
                'location_id' => 1,
                // 'product_status_id' => 1,
                'image'=> 'public/hCXYZK6eJ9nrVomdZG4WouEhbKF0oMvQDDN7BbVT.jpeg'
        ]);

        DB::table('products')->insert([
                'name'=> 'Special Locker SCL',
                'product_model'=>'BL-S-01',
                'price'=>'600',
                'description'=> 'Basic Locker perfect for laptops, tablets, long brown envelopes and much more.',
                'size_id'=> 5,
                'location_id' => 1,
                // 'product_status_id' => 1,
                'image'=> 'public/xOod7jKwHWvLQKsypOvslfVNmaD5AL3rDPeVsvrQ.jpeg'
        ]);
    }
}
