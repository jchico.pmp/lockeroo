<?php

use Illuminate\Database\Seeder;


class BasicLockerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('basic_lockers')->insert([
        		'unique_code'=> 'BL-sb-'.Str::random(5)
        ]);
        DB::table('basic_lockers')->insert([
        		'unique_code'=> 'BL-sb-'.Str::random(5)
        ]);

        DB::table('basic_lockers')->insert([
        		'unique_code'=> 'BL-sb-'.Str::random(5)
        ]);
        DB::table('basic_lockers')->insert([
        		'unique_code'=> 'BL-sb-'.Str::random(5)
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'ML-mb-'.Str::random(5),
                'product_id' => 2,
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'ML-mb-'.Str::random(5),
                'product_id' => 2,
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'ML-mb-'.Str::random(5),
                'product_id' => 2,
        ]);
        DB::table('basic_lockers')->insert([
                'unique_code'=> 'ML-mb-'.Str::random(5),
                'product_id' => 2,
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'BL-bb-'.Str::random(5),
                'product_id' => 3,
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'BL-bb-'.Str::random(5),
                'product_id' => 3,
        ]);
        DB::table('basic_lockers')->insert([
                'unique_code'=> 'BL-bb-'.Str::random(5),
                'product_id' => 3,
        ]);

        DB::table('basic_lockers')->insert([
                'unique_code'=> 'BL-cl-'.Str::random(5),
                'product_id' => 4,
        ]);
        DB::table('basic_lockers')->insert([
                'unique_code'=> 'BL-cl-'.Str::random(5),
                'product_id' => 4,
        ]);
    }
}
