<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentModeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_modes')->insert([
        		'name'=> 'Direct Cash Payment'
        ]);
        DB::table('payment_modes')->insert([
        		'name'=> 'PayPal'
        ]);
    }
}
