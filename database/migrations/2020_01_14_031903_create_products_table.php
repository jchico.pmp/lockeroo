<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('product_model')->unique();
            $table->float('price');
            $table->mediumText('description');
            $table->string('image');

            //foreign key: size_id
            $table->unsignedBigInteger('size_id')->nullable();
            $table->foreign('size_id')
                    ->references('id')->on('sizes')
                    ->onDelete('set null')
                    ->onUpdate('set null');

            //foreign key: location_id
            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')
                    ->references('id')->on('locations')
                    ->onDelete('set null')
                    ->onUpdate('set null');

            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
