<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetalLockermbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metal_lockermbs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('unique_code')->unique();

            //foreign key: product_id
            $table->unsignedBigInteger('product_id')->nullable()->default(2);
            $table->foreign('product_id')
                    ->references('id')->on('products')
                    ->onDelete('set null')
                    ->onUpdate('set null');

             //foreign key: product_status_id
            $table->unsignedBigInteger('product_status_id')->nullable()->default(1);
            $table->foreign('product_status_id')
                ->references('id')
                ->on('product_statuses')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metal_lockermbs');
    }
}
